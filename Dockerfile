FROM adoptopenjdk/openjdk11
EXPOSE 8080
ARG JAR_FILE=target/FootballInfoFinder-1.0-SNAPSHOT.jar
ADD ${JAR_FILE} football.jar
ENTRYPOINT ["java","-jar","/football.jar"]

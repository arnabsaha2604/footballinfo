package com.footballfinder.controller;

import com.footballfinder.entiry.Data;
import com.footballfinder.entiry.TeamStandingRequest;
import com.footballfinder.service.TeamStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("#{'${football.finder.api.base.path}'}")
public class TeamController {

    @Autowired
    private TeamStatisticsService teamStatisticsService;

    @GetMapping(path = "/standing")
    public ResponseEntity<Data> getTeamStandingsByQueryParam(@Valid @RequestBody TeamStandingRequest request) {
       return teamStatisticsService.getTeamStatistics(request);

    }
}


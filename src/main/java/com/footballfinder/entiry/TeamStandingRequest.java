package com.footballfinder.entiry;

import javax.validation.constraints.NotBlank;

public class TeamStandingRequest {
    @NotBlank(message = "Team name is mandatory")
    private String teamName;

    @NotBlank(message = "Country name is mandatory")
    private String countryName;

    @NotBlank(message = "League name  is mandatory")
    private String leagueName;

    public String getTeamName() {
        return teamName;
    }

    public void setTeamName(String teamName) {
        this.teamName = teamName;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    public String getLeagueName() {
        return leagueName;
    }

    public void setLeagueName(String leagueName) {
        this.leagueName = leagueName;
    }
}

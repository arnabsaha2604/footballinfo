package com.footballfinder.entiry;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class Data  {
   @JsonProperty("country_name")
   private String country_name;

    @JsonProperty("league_name")
   private String league_name;

    @JsonProperty("team_name")
   private String team_name;

    @JsonProperty("team_id")
   private String team_id;

    @JsonProperty("overall_league_position")
   private String overall_league_position;

    @JsonProperty("overall_league_payed")
   private String overall_league_payed;

    @JsonProperty("overall_league_W")
   private String overall_league_W;

    @JsonProperty("home_league_position")
   private String home_league_position;

    @JsonProperty("home_league_payed")
   private String home_league_payed;

    @JsonProperty("away_league_position")
   private String away_league_position;

    @JsonProperty("away_league_payed")
   private String away_league_payed;

    public String getCountry_name() {
        return country_name;
    }

    public void setCountry_name(String country_name) {
        this.country_name = country_name;
    }

    public String getLeague_name() {
        return league_name;
    }

    public void setLeague_name(String league_name) {
        this.league_name = league_name;
    }

    public String getTeam_name() {
        return team_name;
    }

    public void setTeam_name(String team_name) {
        this.team_name = team_name;
    }

    public String getTeam_id() {
        return team_id;
    }

    public void setTeam_id(String team_id) {
        this.team_id = team_id;
    }

    public String getOverall_league_position() {
        return overall_league_position;
    }

    public void setOverall_league_position(String overall_league_position) {
        this.overall_league_position = overall_league_position;
    }

    public String getOverall_league_payed() {
        return overall_league_payed;
    }

    public void setOverall_league_payed(String overall_league_payed) {
        this.overall_league_payed = overall_league_payed;
    }

    public String getOverall_league_W() {
        return overall_league_W;
    }

    public void setOverall_league_W(String overall_league_W) {
        this.overall_league_W = overall_league_W;
    }

    public String getHome_league_position() {
        return home_league_position;
    }

    public void setHome_league_position(String home_league_position) {
        this.home_league_position = home_league_position;
    }

    public String getHome_league_payed() {
        return home_league_payed;
    }

    public void setHome_league_payed(String home_league_payed) {
        this.home_league_payed = home_league_payed;
    }

    public String getAway_league_position() {
        return away_league_position;
    }

    public void setAway_league_position(String away_league_position) {
        this.away_league_position = away_league_position;
    }

    public String getAway_league_payed() {
        return away_league_payed;
    }

    public void setAway_league_payed(String away_league_payed) {
        this.away_league_payed = away_league_payed;
    }

    @Override
    public String toString() {
        return "Data{" +
                "country_name='" + country_name + '\'' +
                ", league_name='" + league_name + '\'' +
                ", team_name='" + team_name + '\'' +
                ", team_id='" + team_id + '\'' +
                ", overall_league_position='" + overall_league_position + '\'' +
                ", overall_league_payed='" + overall_league_payed + '\'' +
                ", overall_league_W='" + overall_league_W + '\'' +
                ", home_league_position='" + home_league_position + '\'' +
                ", home_league_payed='" + home_league_payed + '\'' +
                ", away_league_position='" + away_league_position + '\'' +
                ", away_league_payed='" + away_league_payed + '\'' +
                '}';
    }
}

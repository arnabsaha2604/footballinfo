package com.footballfinder.service;

import com.footballfinder.entiry.Data;
import com.footballfinder.entiry.TeamStandingRequest;
import org.springframework.http.ResponseEntity;

public interface TeamStatisticsService {
    ResponseEntity<Data> getTeamStatistics(TeamStandingRequest request) ;
}

package com.footballfinder.service.impl;

import com.footballfinder.entiry.Country;
import com.footballfinder.entiry.Data;
import com.footballfinder.entiry.League;
import com.footballfinder.entiry.Team;
import com.footballfinder.entiry.TeamStandingRequest;
import com.footballfinder.service.TeamStatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Service
public class TeamStatisticsServiceImpl implements TeamStatisticsService {
    @Autowired
    private RestTemplate restTemplate;

    @Value( "${football.finder.football.apiKey}")
    private String apiKey;


    private String footballApiUrl = "https://apiv2.apifootball.com/";



    private String url;

    @Override
    public ResponseEntity <Data> getTeamStatistics(TeamStandingRequest request) {
        // Check all the details are provided

            Country  country = getCountryByName(request.getCountryName());
            if(Objects.isNull(country)) throw  new RuntimeException("No country Found");
            League league =getLeagueDetailsByCountry(country.getCountry_id(),request.getLeagueName());
            if(Objects.isNull(league)) throw  new RuntimeException("No league details Found for "+request.getLeagueName());
            Team team = getTeamDetailsByLeagueId(league.getLeague_id(),request.getTeamName());
            if(Objects.isNull(team)) throw  new RuntimeException("No team details Found for "+request.getTeamName());
            List<Data> leagueStanding = getStandingByLeague(league.getLeague_id());
            Data data = leagueStanding
                    .stream()
                    .filter(l ->l.getTeam_name().equalsIgnoreCase(team.getTeam_name()))
                    .findFirst()
                    .get();
            return ResponseEntity.ok(data);

    }

    private  Country getCountryByName(String country) {
        return getCountries()
                .stream()
                .filter(c->c.getCountry_name().equalsIgnoreCase(country))
                .findFirst().orElseGet(null);

    }

    private  League getLeagueDetailsByCountry(String countryId ,String leagueName) {
        return getLeagueDetails(countryId)
                .stream()
                .filter(c->c.getLeague_name().equalsIgnoreCase(leagueName))
                .findFirst().orElseGet(null);

    }

    private  Team getTeamDetailsByLeagueId(String leagueId ,String teamName)  {
        return getTeamDetails(leagueId)
                .stream()
                .filter(c->c.getTeam_name().equalsIgnoreCase(teamName))
                .findFirst().orElseGet(null);

    }


    private  List<Country> getCountries() {
        Map<String,String> queryParams = new HashMap <>();
        queryParams.putIfAbsent("action","get_countries");
        queryParams.putIfAbsent("APIkey",apiKey);
        ResponseEntity<Country[]> responseEntity = restTemplate.getForEntity(buildUrl(queryParams),Country[].class);
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        }else{
           throw new HttpClientErrorException(HttpStatus.BAD_REQUEST) ;
        }
    }

    private List<League> getLeagueDetails(String countryId) {
        Map<String,String> queryParams = new HashMap <>();
        queryParams.putIfAbsent("action","get_leagues");
        queryParams.putIfAbsent("country_id",countryId);
        queryParams.putIfAbsent("APIkey",apiKey);
        ResponseEntity<League[]> responseEntity = restTemplate.getForEntity(buildUrl(queryParams),League[].class);
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        }else{
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST) ;
        }
    }

    private List<Team> getTeamDetails(String leagueId){
        Map<String,String> queryParams = new HashMap <>();
        queryParams.putIfAbsent("action","get_teams");
        queryParams.putIfAbsent("league_id",leagueId);
        queryParams.putIfAbsent("APIkey",apiKey);
        ResponseEntity<Team[]> responseEntity = restTemplate.getForEntity(buildUrl(queryParams),Team[].class);
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        }else{
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST) ;
        }
    }

    private List<Data> getStandingByLeague(String leagueId)  {
        Map<String,String> queryParams = new HashMap <>();
        queryParams.putIfAbsent("action","get_standings");
        queryParams.putIfAbsent("league_id",leagueId);
        queryParams.putIfAbsent("APIkey",apiKey);
        ResponseEntity<Data[]> responseEntity = restTemplate.getForEntity(buildUrl(queryParams),Data[].class);
        if(responseEntity.getStatusCode().is2xxSuccessful()){
            return Arrays.asList(Objects.requireNonNull(responseEntity.getBody()));
        }else{
            throw new HttpClientErrorException(HttpStatus.BAD_REQUEST) ;
        }
    }
    private String buildUrl(Map<String,String> queryMap){
        StringBuilder builder = new StringBuilder();
        builder.append(footballApiUrl);
        builder.append("?");
        queryMap.forEach((k,v)->{
            builder.append(k);
            builder.append("=");
            builder.append(v);
            builder.append("&");
        });
        String queryUrl = builder.toString();
        return queryUrl.substring(0,queryUrl.length()-1);

    }


}

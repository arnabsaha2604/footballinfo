
##Build Project 
   Run below command to build the project 
   ```
    mvn clean install 
```

## Build Docker Imges
  
   Build docker image . It will create a new new docker image.
   ```aidl
    docker build --tag=footballinfo:latest .
```

## Run Docker image locally 
  Application will start at host machine 8080 port
```aidl
    docker run -p80:8080 footballinfo:latest
```


